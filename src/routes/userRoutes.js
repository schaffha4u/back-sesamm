module.exports = (app, db) => {
  /**
   * @api {get} /users Get all users with pagination
   * @param {pageNumber} pageNumber
   * @param {pageSize} pageSize
   *
   * @apiSuccess {Object} List of users with count of total users
   */
  app.get("/users", async (req, res) => {
    try {
      const pageNumber = req.query.pageNumber || 0;
      const pageSize = req.query.pageSize || 15;
      let userList = await db.user.findAndCountAll({
        include: [{ model: db.address, as: "addresses" }],
        offset: pageNumber * pageSize,
        limit: pageSize,
      });
      res.send(userList);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error });
    }
  });

  /**
   * @api {get} /users/:id Get user by id
   *
   * @apiSuccess {Object} User
   */
  app.get("/user/:userid", async (req, res) => {
    try {
      let user = await db.user.findOne({
        include: [{ model: db.address, as: "addresses" }],
        where: {
          id: parseInt(req.params.userid),
        },
      });
      res.send(user);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error });
    }
  });
};
