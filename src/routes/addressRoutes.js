module.exports = (app, db) => {
  /**
   * @api {get} /addresses Get all addresses
   *
   * @apiSuccess {Object} List of addresses
   */
  app.get("/addresses", async (req, res) => {
    try {
      let addressList = await db.address.findAll();
      res.send(addressList);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error });
    }
  });

  /**
   * @api {get} /addresses/:id Get address by id
   * @apiSuccess {Object} Address
   * @apiError {Object} Error
   */
  app.get("/address/:id", async (req, res) => {
    try {
      let address = await db.address.findOne({
        where: {
          id: req.params.adressId,
        },
      });
      res.send(address);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error });
    }
  });
};
