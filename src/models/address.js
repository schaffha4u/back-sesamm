module.exports = (sequelize, DataTypes) =>
  /**
   * Define the address model in the database
   * @param {Sequelize} sequelize instance
   * @param {DataTypes} list of dataTypes in the database
   */
  {
    return sequelize.define(
      "addresses",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          allowNull: false,
        },
        streetNumber: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        street: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        postalCode: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        city: {
          type: DataTypes.STRING(30),
          allowNull: false,
        },
        userId: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "users",
            key: "id",
          },
        },
      },
      {
        timestamps: false,
      }
    );
  };
