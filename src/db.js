const { Sequelize, DataTypes } = require("sequelize");

/**
 * Create the database connection
 */
let sequelize = new Sequelize("expenserdb", "user", "pass", {
  dialect: "sqlite",
  storage: "sqlite:relativePath/../database.db",
});

/**
 * Import all models of the database
 */
let User = require(__dirname + "\\models\\user")(sequelize, DataTypes);
let Address = require(__dirname + "\\models\\address")(sequelize, DataTypes);

/**
 * Declare the OneToMany associations between the models User and Address
 */
User.hasMany(Address, { foreignKey: "userId" });

/**
 * Export the sequelize instance and the models associated to it
 */
module.exports.user = User;
module.exports.address = Address;
module.exports.sequelize = sequelize;
