const express = require("express");
const cors = require("cors");
const request = require("supertest");
const assert = require("assert");
const app = express();
app.use(cors());
const db = require("./db.js");

/* Import routes from other files */
require("./routes/userRoutes")(app, db);
require("./routes/addressRoutes")(app, db);

app.listen(5500, () => {
  console.log("Serveur démarré sur le port 5500");
});

module.exports = app;
