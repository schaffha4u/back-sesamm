const assert = require("assert");
const expect = require("chai").expect;
const request = require("supertest");
const app = require("../src/index.js");

describe("Unit tests for GET /users route", () => {
  it("should return OK status", () => {
    return request(app)
      .get("/users")
      .then((response) => {
        assert.equal(response.status, 200);
      });
  });

  it("should return a list of users", (done) => {
    request(app)
      .get("/users")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });
});

describe("Unit tests for GETE /user/:id route", () => {
  it("should return OK status", () => {
    return request(app)
      .get("/user/1")
      .then((response) => {
        assert.equal(response.status, 200);
      });
  });
  it("sould return first user", (done) => {
    request(app)
      .get("/user/1")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.equal(res.body.firstName, "Jerri");
        done();
      });
  });
});
