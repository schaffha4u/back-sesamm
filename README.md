
# Back SESAMm

Back-end application for a technical test in order to get an apprentice at SESAMm (Metz) for the Master 2 GI at Metz. 
It has been realised between 05/28/2022 and 05/29/2022.

## Tech Stack

**Server** : Node, Express, Sequelize (ORM)

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/schaffha4U/back-sesamm.git
```
Go to the project directory
```bash
  cd back-sesamm
```
Install dependencies

```bash
  npm install
```

Start the server

```bash
  nodemon src/index.js
```

## Authors

- Bastien SCHAFFHAUSER

